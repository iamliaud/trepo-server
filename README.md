# trepo-server

[![Coverage Status](https://coveralls.io/repos/gitlab/iamliaud/trepo-server/badge.svg?branch=master)](https://coveralls.io/gitlab/iamliaud/trepo-server?branch=master)

## Technologies requises
![docker_img](https://upload.wikimedia.org/wikipedia/commons/thumb/4/4e/Docker_%28container_engine%29_logo.svg/1280px-Docker_%28container_engine%29_logo.svg.png)

## Étapes à suivre
1. Cloner le dépôt
```sh
$ git clone https://gitlab.com/iamliaud/trepo-server
```
2. Aller dans le dossier trepo-server
```sh
$ cd trepo-server/
```
3. Monter l'environnement Docker
```sh
$ docker-compose up
```

## Comment accéder à l'API ?
```sh
http://localhost:3000
```

## Swagger de l'API
Le Swagger est disponible à l'adresse : 
```sh
http://localhost:3000/api
```